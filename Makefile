GPROJECT_ID = origin-demo-212611
IMAGE = og-bash-echo
PROJECT = og-bash-backend

cloud/init:
	gcloud config set project  $(GPROJECT_ID)
	gcloud config set compute/zone australia-southeast1
	gcloud auth configure-docker
	gcloud container clusters get-credentials cluster-1 --zone=australia-southeast1
build/local:
	docker build -t $(IMAGE) .
	docker tag $(IMAGE) gcr.io/$(GPROJECT_ID)/$(IMAGE):v2
build/gcloud:
	gcloud builds submit --config cloudbuild.yaml .
build/glcoud-cli:
	gcloud builds submit --tag gcr.io/$(GPROJECT_ID)/$(IMAGE) .
push/manual:
	docker push gcr.io/$(GPROJECT_ID)/$(IMAGE):v2
create/deployment:
	kubectl run $(PROJECT) --image=gcr.io/$(GPROJECT_ID)/$(IMAGE):v2 --port 80
deploy/gcloud:
	kubectl set image deployment/$(PROJECT) $(PROJECT)=gcr.io/$(GPROJECT_ID)/$(IMAGE):v2
encrypt:
	gcloud kms encrypt --plaintext-file=secrets.json --ciphertext-file=secrets.json.enc --location=global --keyring=og --key=ogkey
